		const iptKey = document.getElementById('iptKey');
		const iptVal = document.getElementById('iptVal');
		const btnIns = document.getElementById('btnIns');
		const clear = document.getElementById('clear');
		const getKey = document.getElementById('getKey');
		const btnRemove = document.getElementById('btnRemove');
		const myLocalStorage = document.getElementById('myLocalStorage');

		btnIns.onclick = function () {
			const key = iptKey.value;
			const val = iptVal.value;

			if (key && val) {
				localStorage.setItem(key,val);
				location.reload();
			}
		}
		btnRemove.onclick = function () {
			const key = getKey.value;

			if (key) {
				localStorage.removeItem(key);
				location.reload();
			}
		}
		clear.onclick = function () {
			localStorage.clear();
			location.reload();
		}

		for (let i = 0; i < localStorage.length; i++) {
			const key = localStorage.key(i);
			const val = localStorage.getItem(key);
			const link = '<a class="btn btn-primary btn-lg m-3" href="'+val+'" target="_blank" title="'+key+'">'+key+'</a>';

			myLocalStorage.innerHTML += link;
		}
		// random gradient
		function generate() {

			var hexValues = ["1","2","3","4","5","6","7","8","9","a","b","c","d","e"];

			function populate(a) {
				for ( var i = 0; i < 6; i++ ) {
					var x = Math.round( Math.random() * 13 );
					var y = hexValues[x];
					a += y;
				}
				return a;
			}

			var newColor1 = populate('#');
			var newColor2 = populate('#');
			var angle = Math.round( Math.random() * 360 );

			var gradient = "linear-gradient(" + angle + "deg, " + newColor1 + ", " + newColor2 + ")";

			document.getElementById("bg-color").style.background = gradient;

		}

		document.onload = generate();
